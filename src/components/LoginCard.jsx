import React from 'react'
import {Card, Button} from 'react-bootstrap'

const LoginCard = () => {
    return (
        <Card>
            <Card.Img variant="top" src="holder.js/100px180" />
            <Card.Body>
                <Card.Text>
                    Please Login
                </Card.Text>
                <Button variant="primary" size="sm">Login with google</Button>
            </Card.Body>
        </Card>
    )
}

export default LoginCard
