import React from 'react'
import { Nav, Navbar, Form, FormControl, Button, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const NavMenu = () => {
    return (
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand href="#home">AMS Redux</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link as={Link} to="/">Home</Nav.Link>
                        <Nav.Link as={Link} to="/article">Article</Nav.Link>
                        <Nav.Link as={Link} to="/athour">Author</Nav.Link>
                        <Nav.Link as={Link} to="/category">Category</Nav.Link>
                    </Nav>
                    <Form className="d-flex">
                        <FormControl type="text" placeholder="Search" className="me-sm-2" />
                        <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default NavMenu
