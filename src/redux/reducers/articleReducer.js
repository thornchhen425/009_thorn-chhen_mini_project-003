import { articleType as actionType} from "../actions/actionType"

const initialState = {
    articles: [],
    article: []
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

    case actionType.FETCH_ALL_ARTICLE:
        return { ...state, articles: [...payload] }

    case actionType.FETCH_ARTICLE_BY_ID:
        return { ...state, article: payload }

    case actionType.POST_ARTICLE:
        return { ...state, payload }

    case actionType.DELETE_ARTICLE:
        return {...state, articles: state.articles.filter(article=>article._id !== payload._id )}
    default:
        return state
    }
}
