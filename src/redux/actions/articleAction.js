import { deleteArticle, fetchAllArticle } from "../../services/article_service"
import { articleType as type } from "./actionType"
import api from "../../utils/api"


export const onFetchArticle = () => async dispatch=> {
   let response = await fetchAllArticle()
   dispatch({
       type: type.FETCH_ALL_ARTICLE,
       payload: response.data
   })
   return Promise.resolve(response.message)
}

// export const onFetchArticleById = (id) => async dispatch=> {
//    let response = await fetchArticleById(id)
//    dispatch({
//        type: type.FETCH_ARTICLE_BY_ID,
//        payload: response.data
//    })
//    return Promise.resolve(response.message)
// }

export const fetchArticleById = (id) => async dp =>{
    let response = await api.get(`articles/${id}`)
    return dp({
        type: type.FETCH_ARTICLE_BY_ID,
        payload: response.data.data
    })
}

// export const onDeleteArticle = (id) => async dispatch=> {
//    let response = await deleteArticle(id)
//    dispatch({
//        type: type.DELETE_ARTICLE,
//        payload: id
//    })
//    return Promise.resolve(response.message)
// }

export const onDeleteArticle = (id) => async dp => {
    let response = await api.delete(`articles/${id}`)
    return dp({
        type: type.DELETE_ARTICLE,
        payload: response.data.data
    })
}

export const postArticle = (article) => async dp => {
    let response = await api.post(`articles`, article)
    return dp({
        type: type.POST_ARTICLE,
        payload: response.data.message
    })
}

