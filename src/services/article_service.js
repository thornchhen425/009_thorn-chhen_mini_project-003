import api from "../utils/api"
import {articleType} from '../redux/actions/actionType'

export const fetchAllArticle = async () => {
    let response = await api.get('articles')
    return response.data
}

// export const deleteArticle = async (id) => {
//     let response = await api.delete(`articles/${id}`)
//     return response.data.message 
// }

// export const fetchArticleById = (id) => async dp =>{
//     let response = await api.get(`articles/${id}`)
//     return dp({
//         type: articleType.FETCH_ARTICLE_BY_ID,
//         payload: response.data.data
//     })
// }

// export const postArticle = async (article) => {
//     let response = await api.post(`articles/`, article)
//     return response.data.message 
// }
