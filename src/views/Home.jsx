import React, { useEffect } from 'react'
import ArticleCard from '../components/ArticleCard'
import LoginCard from '../components/LoginCard'
import { Row, Col, Container, Button } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { onFetchArticle } from '../redux/actions/articleAction'

const Home = () => {

    const articles = useSelector(state => state.articleReducer.articles)
    const dispatch = useDispatch()

    const onFetch = bindActionCreators(onFetchArticle, dispatch)

    useEffect(() => {
        onFetch().then(console.log)
    }, [])

    return (
        <Container>
           <div className="my-4">
           <h3>Category</h3>
                <Button variant="outline-dark" size="sm">
                    <strong>metoo</strong>
                </Button>{' '}
                <Button variant="outline-dark" size="sm">
                    <strong>sports</strong>
                </Button>
           </div>
            <Row>
                {
                    articles.map(article =>
                        <Col key={article._id} md={3} className="my-2">
                            <ArticleCard article={article} />
                        </Col>)
                }
            </Row>
        </Container>
    )
}

export default Home
