import React, { useState } from 'react'
import { Form, Container, Button, Row, Col } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { bindActionCreators} from 'redux'
import {postArticle} from '../redux/actions/articleAction'

const PostArticle = () => {

    const [title, setTitle] = useState("")
    const [author, setAuthor] = useState("")
    const [description, setDescription] = useState("")
    const [imageURL, setImageURL] = useState("")

    const dispatch = useDispatch()

    const onPostArticle = bindActionCreators(postArticle, dispatch)
    

    const onSave = (e) => { 
        e.preventDefault()
        let article = {
            title, description
        }
        onPostArticle(article).then(message=>alert(message.payload))
    }

    return (
        <Container>
            <h3 className="my-4">Add Article</h3>
            <Row>
                <Col md={8}>
                    <Form>
                        <Form.Group className="mb-3" controlId="title">
                            <Form.Label>Title</Form.Label>
                            <Form.Control
                                value={title}
                                onChange={(e) => setTitle(e.target.value)}
                                type="text"
                                placeholder="Title"
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="author">
                            <Form.Label>Author</Form.Label>
                            <Form.Control
                                value={author}
                                onChange={(e) => setAuthor(e.target.value)}
                                type="text"
                                placeholder="Author"
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="description">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                value={description}
                                onChange={(e) => setDescription(e.target.value)}
                                as="textarea"
                                placeholder="Description"
                            />
                        </Form.Group>

                        <Button onClick={onSave} variant="primary" type="button">
                            Post
                        </Button>
                    </Form>
                </Col>

                <Col md={4}>
                    <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Import image</Form.Label>
                        <Form.Control
                            type="file"
                            value={imageURL}
                            onChange={(e) => setImageURL(e.target.value)}
                        />
                    </Form.Group>
                </Col>
            </Row>
        </Container>
    )
}

export default PostArticle
