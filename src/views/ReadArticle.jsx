import React, { useEffect } from 'react'
import { Container } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { fetchArticleById } from '../redux/actions/articleAction'

const ReadArticle = () => {

    const { id } = useParams()
    const dispatch = useDispatch()
    const article = useSelector(state => state.articleReducer.article)
    console.log("ARTICLE:", article);

    useEffect(() => {
        dispatch(fetchArticleById(id))
        console.log("ID", id);
        console.log("ARTICLE:", article);
    }, [])

    return (
        <Container>
            <h3>Read Article</h3>
                <h1>{article.title}</h1>
                <img src={article.image} />
                <p>{article.description}</p>
        </Container>
    )
}

export default ReadArticle
