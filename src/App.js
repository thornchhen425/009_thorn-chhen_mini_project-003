import NavMenu from "./components/NavMenu";
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from "./views/Home";
import PostArticle from "./views/PostArticle";
import ReadArticle from "./views/ReadArticle";


function App() {
  return (
    <Router>
      <NavMenu />
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/article" component={PostArticle} />
          <Route path="/read/:id" component={ReadArticle} />
        </Switch>
    </Router>
  );
}

export default App;
